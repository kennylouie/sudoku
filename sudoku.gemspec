Gem::Specification.new do |s|
  s.name = "sudoku"
  s.version = "0.1.0"
  s.authors = ["kennylouie"]
  s.email = ["kennylouie@protonmail.ch"]
  s.summary = "sudoku ai"
  s.description = "solves sudoku things"
  s.homepage = "http://github.com/kennylouie/sudoku"

  s.files = Dir.glob("lib/**/*.rb")
  s.test_files = Dir.glob("{test,spec}/**/*.rb")

  s.add_development_dependency "rspec", "~> 2.5"
end
