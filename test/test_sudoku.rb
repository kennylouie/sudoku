require "test/unit"
require "sudoku"

class TestSudoku < Test::Unit::TestCase
  def test_version_string
    assert_equal Sudoku.version_string, "Sudoku version #{Sudoku::VERSION}"
  end

  def test_addition
    assert_equal Sudoku.addition, 5*5
  end

  def test_another_mod
    assert_equal Sudoku.speak, "hello, world"
  end
end
