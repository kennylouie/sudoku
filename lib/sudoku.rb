require "sudoku/version"
require "sudoku/addition"
require "sudoku/person"

module Sudoku
  def self.version_string
    "Sudoku version #{Sudoku::VERSION}"
  end

  def self.addition
    Sudoku::Square(5)
  end

  def self.speak
    Sudoku::Person.new.speak()
  end
end
